# Implementation notes
The extension has two JavaScript parts: a background script and a content script.

## Background script
The background script _bg.js_ does two things:

- register the content script so it runs on all displayed email messages,
- set up a listener for messages within the extension; received message contents are put on the system clipboard.

### Registering the content script
The content script should run at every displayed mail message: it is registered under
the `browser.messageDisplayScripts` API. This works according to the second set of examples on [this documentation page](https://webextension-api.thunderbird.net/en/91/changes/beta82.html#messagedisplayscripts-tabs).

## The content script
The content script _msgscript.js_ runs on every opened email, whether it's read in the main window, a separate window, or a tab. The API used here is [the one for Content scripts](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Content_scripts#webextension_apis) as well as the standard JavaScript for the DOM of a web page, specifically the syntax to create event listeners.

It turns out that the `select` event doesn't work, so we listen for `mouseup` events (releasing the mouse button) and then we check whether any text is currently selected.

### Color of selection range
When black text against a white background is selected, the white looks like color #b3d8ff and the black stays black, which can only be explained by saying this is in fact not an overlay with a certain color and transparency, but it is a _background color_: the letters are the foreground and are not affected by the coloring.
