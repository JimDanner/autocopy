Text on [addons.mozilla.org](https://addons.thunderbird.net/addon/autocopy-78/) (AMO)

# Autocopy 78+
Automatically copy any text in an email when the text is selected.

## Detail
Select any text inside an email you are reading. It will be copied to the system clipboard; you save yourself a `Ctrl`+`C`, `Cmd`+`C`, right-click + Copy or whatever you're normally doing. The text is copied as plain text, without any formatting.

Works on the body (not the subject, sender etc.) of emails that are being read in the main Thunderbird window, in a separate window, or in a tab. Does not work in a "New email" composition window.

Many predecessors exist, most with a lot more functions and options, but they don't work on Thunderbird from version 78, when the extension system changed to _MailExtensions_:

- [AutoCopy](https://addons.thunderbird.net/en-US/thunderbird/addon/autocopy), the original, for Thunderbird 1.0-18
- [AutoCopy 2](https://addons.thunderbird.net/en-US/thunderbird/addon/autocopy-2), for Thunderbird 13.0-49.0
- [AutoCopy_rebirth](https://addons.thunderbird.net/en-US/thunderbird/addon/autocopy-rebirth), for Thunderbird 13.0-60

## Categories

- Message and News Reading
- Import/Export

## Tags

- autocopy
- auto-copy
- auto copy
- copy
- text
- plain text
- copy text
- copy mail
- copy contents

# Links

- [Add-on home page](https://gitlab.com/JimDanner/autocopy)
- [Support site](https://gitlab.com/JimDanner/autocopy/-/issues)
