/* Fade out the selection shading */

/* Selection adds shading to all (parts of) text nodes inside the selected area.
 * Shading block elements risks shading a full-width element like a <p>, which
 * differs from what the window manager does, so it should be avoided.  */

function wrapTextInSpan(node, classname) {
    let spn = document.createElement('span');
    spn.className = classname;
    spn.appendChild(node.cloneNode());
      /* alternative: spn.textContent = node.textContent
       * (but it feels 'cleaner' to not work on content but purely on DOM objects) */
    node.parentNode.replaceChild(spn, node);
    return spn;
}

function putNodeInClass(node, classname) {
	// only text nodes; skip if it's whitespace between <tr> and <td>, <tbody> and <tr> etc.
	if (node.nodeType === node.TEXT_NODE && 
		!['table', 'tbody', 'tr'].includes(node.parentNode.nodeName.toLowerCase()))
		node = wrapTextInSpan(node, classname);
	return node;
}

/**
 * @param {Node} node
 * @param {Range} range
 * @param {string} side
 */
function selectedPartOfNode(node, range, side) {
	// side = at which side of the selection are we working now?
	// this function may pollute the DOM with empty text nodes, but who cares
	if (node.nodeType !== node.TEXT_NODE) return node;
		// non-text nodes can't be partially selected
	switch (side) {
		case "left":
			return node.splitText(range.startOffset);
		case "right":
			node.splitText(range.endOffset);
			return node;
		case "whole":
			node.splitText(range.endOffset);
			return node.splitText(range.startOffset);
	}
}

function putSelectionInClass(classname) {
	let rng = document.getSelection().getRangeAt(0);
	let [current, last] = [rng.startContainer, rng.endContainer];
	// if the container is an element, descend to the child node first/last in range
	let startsInElement = (current.nodeType === current.ELEMENT_NODE);
	if (startsInElement)
		current = current.childNodes[rng.startOffset];
	let endsInElement = (last.nodeType === last.ELEMENT_NODE);
	if (endsInElement)
		last = last.childNodes[rng.endOffset - 1];

	// the start node may be selected only partially (if its container is a text node)
	if (current === last) {
		if (!startsInElement)
			current = selectedPartOfNode(current, rng, "whole");
		putNodeInClass(current, classname);
		return;
	}
	// descend to a leave node
	while (current.hasChildNodes())
		current = current.firstChild;	// descend the tree as deep as possible
	while (last.hasChildNodes())
		last = last.lastChild;	// descend the tree as deep as possible

	if (!startsInElement)
		current = selectedPartOfNode(current, rng, "left");
	current = putNodeInClass(current, classname);
	
	/* Traverse the leaves. Procedure: from current, go to the right; when hitting the wall,
	 * go up and to the right; go down and then right; at last, stop */
	while (true) {
		while (!current.nextSibling)
			current = current.parentNode;  // up in the tree until there's a right-neighbor
		current = current.nextSibling;
		while (current.hasChildNodes())
			current = current.firstChild;	// descend the tree as deep as possible
		if (current === last) break;
		current = putNodeInClass(current, classname);
	}
	// the end node may be selected only partially (if its container is a text node)
	if (!endsInElement)
		last = selectedPartOfNode(last, rng, "right");
	putNodeInClass(last, classname);
}

function fadeSelection() {	
	// fade the selection color (setTimeout > 1000/framerate to ensure rendering inbetween)
	putSelectionInClass('slctd');
	document.getSelection().collapseToStart();  // deselect the selection
	setTimeout(() => {
		document.querySelectorAll('.slctd').forEach(el => {
			el.classList.replace('slctd', 'unslctd');
		})
	}, 100);
}

let stle = document.createElement('style');
stle.innerHTML = `.slctd {background-color: #b3d6ff;}
.unslctd {background-color: transparent;
 transition: background-color 1s ease-in-out;}`;
document.head.appendChild(stle);
