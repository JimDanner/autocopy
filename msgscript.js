'use strict';

/* Content script
 * Listens for potential selection events; if there is a selection,
 * sends the selected text to the background script
 */

async function copySelection() {
	const sel = document.getSelection().toString();
	if (!sel) return;

	// send the selected text to the background script for insertion into the clipboard
	let result = await messenger.runtime.sendMessage(sel);
	if (result == 1) fadeSelection();
}

document.body.addEventListener('mouseup', copySelection);


/* Unrelated addition, for testing: open calendar invitation on screen */
document.querySelector('#imipHTMLDetails')?.setAttribute('open', '');
