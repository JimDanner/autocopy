'use strict';

/* Background script
 * Registers the script that runs inside displayed mail messages;
 * listens for messages from that script to put text on the clipboard
 */
console.log("Loading extension Autocopy");

const contentScripts = {
	js: [
		{file: "msgscript.js"},
		{file: "fadeselection.js"}
	]
};
const defaultOptions = {
	deselectAfterCopy: false
};
messenger.storage.local.get(defaultOptions)
	.then(result => {
		this.options = result;
		console.log("Settings: ", options);
	})
	.catch((err) => {
		this.options = defaultOptions;
		console.log("Error reading settings: ", err);
	});


function toSystemClipboard(text, source, reply) {
	navigator.clipboard.writeText(text)
		.then(() => reply(this.options.deselectAfterCopy ? 1 : 0))
		.catch(e => {
			console.log(e);
			reply(0);
		});
	return true;  // allow response to be sent
}


browser.messageDisplayScripts.register(contentScripts)
	.catch(console.log);

browser.runtime.onMessage.addListener(toSystemClipboard);

