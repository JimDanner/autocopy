// Workaround suggested by Mozilla for non-working storage access
const bg = window.browser.extension.getBackgroundPage();

let pageTitle = document.getElementById('pageTitle');
let checkDeselectAfterCopy = document.getElementById('deselectAfterCopy');
let labelDeselectAfterCopy = document.getElementById('labelDeselectAfterCopy');

pageTitle.textContent = bg.messenger.i18n.getMessage("settingsHeader");
labelDeselectAfterCopy.textContent = bg.messenger.i18n.getMessage("deselectAfterCopy");

checkDeselectAfterCopy.checked = bg.options.deselectAfterCopy;
checkDeselectAfterCopy.addEventListener('CheckboxStateChange', () => {
	bg.options.deselectAfterCopy = checkDeselectAfterCopy.checked;
	bg.messenger.storage.local.set(bg.options);
})

