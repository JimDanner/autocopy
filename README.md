# Autocopy

A very basic rebuild of the old *Autocopy* extension that has existed in various versions, for the MailExtensions API (Thunderbird 78+). Thunderbird-specific.

## Usage
Select any text inside an email you are reading. It will be copied to the system clipboard; you save yourself a <kbd>Ctrl</kbd>+<kbd>C</kbd>, <kbd>Cmd</kbd>+<kbd>C</kbd>, right-click + Copy or whatever you're normally doing. The text is copied as plain text, without any formatting.

## Further development
The add-on now satifies all my needs, but earlier versions had a lot more functionality:

- Auto-copying the message subject, sender, recipient etc.
- Auto-copying in a mail _compose_ window (I personally don't want that because it makes editing more difficult)
- Working in Firefox and SeaMonkey
- Buttons in the status bar and toolbar
- Being automatically inactive on certain mails (a blacklist)
- A context menu that could appear after something is selected, with various actions
- Various options:
    * Copying with or without the text format
    * Automatic deselection of the text after it has been auto-copied
    * Paste on middle-click
    * Keyboard shortcuts

I think some of these things _can_ be implemented, but it would take time. If they are important to you, please let me know - or better, implement them and make a pull request.

## Thunderbird version compatibility
I re-wrote the add-on from scratch to work in the MailExtensions system introduced with Thunderbird 78. One of the APIs used, `messageDisplayScripts`, according to [the documentation](https://webextension-api.thunderbird.net/en/91/messageDisplayScripts.html) "first appeared in Thunderbird 82 and was backported to Thunderbird 78.4". Thus, this add-on should work in **versions 78.4 and higher**.

## Extension permissions
This extension asks for two permissions when it is installed:

- _Input data to the clipboard_: when text is autocopied, it needs to be put on the system clipboard
- _Read and modify your email messages as they are displayed to you_: a permission to read the mail messages would have been sufficient, but the system doesn't allow for such fine-grained rights. Also, the auto-copying works with a JavaScript function that's run upon mouse-button release; perhaps setting such an event listener on the email is considered "modifying" the email. Anyway, no actual modifications are made to your email displays, nor to their stored contents.
